package com.example.fragtofrag

interface Communicator {
    fun loginSuccessful(s: String)
    fun loginUnSuccessful(s: String)
    fun gotoLoginFragment()
}