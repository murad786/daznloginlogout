package com.example.fragtofrag.fragemnents

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.fragtofrag.R
import kotlinx.android.synthetic.main.fragment_b.view.*
import android.content.Intent
import com.example.fragtofrag.Communicator
import com.example.fragtofrag.MainActivity


/**
 * A simple [Fragment] subclass.
 * Use the [BFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class BFragment : Fragment() {
   var displayMessage:String? = ""
    private lateinit var communicator: Communicator
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_b, container, false)
        displayMessage = arguments?.getString("login")
        view.result.text = displayMessage
        communicator = activity as Communicator
        view.Logout.setOnClickListener {
           communicator.gotoLoginFragment()
            //val intent = Intent(context?.applicationContext,MainActivity::class.java)
            //startActivity(intent)

        }
        return view
    }


}