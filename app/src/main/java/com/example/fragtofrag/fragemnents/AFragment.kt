package com.example.fragtofrag.fragemnents

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.fragtofrag.R
import kotlinx.android.synthetic.main.fragment_a.*
import com.example.fragtofrag.Communicator
import kotlinx.android.synthetic.main.fragment_a.view.*
import kotlinx.android.synthetic.main.fragment_b.*
import kotlinx.android.synthetic.main.fragment_b.view.*


/**
 * A simple [Fragment] subclass.
 * Use the [AFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AFragment : Fragment() {
    val userName: String = "Murad"
    val passWord: String = "1234"
    var displayMessage:String? = ""
    private lateinit var communicator: Communicator
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_a, container, false)
        displayMessage = arguments?.getString("failed")
        view.failed.text = displayMessage
        communicator = activity as Communicator
        view.button.setOnClickListener {
            Log.i("Button", "Clicked")
            Log.i("User", editTextTextPersonName.text.toString())
            Log.i("Password", editTextTextPassword.text.toString())
            if (userName.equals(editTextTextPersonName.text.toString())
                && passWord.equals(editTextTextPassword.text.toString())
            ) {
                Log.i("Login", "Successful")
                communicator.loginSuccessful("LoginSuccessful")

            } else {
                Log.i("Login", "UnSuccessful")
               communicator.loginUnSuccessful("Either username or password is wrong")

            }
        }

         return view
    }
}

