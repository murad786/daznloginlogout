package com.example.fragtofrag

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.fragtofrag.fragemnents.AFragment
import com.example.fragtofrag.fragemnents.BFragment
import kotlinx.android.synthetic.main.fragment_b.*

class MainActivity : AppCompatActivity(),Communicator {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val aFragment = AFragment()

        supportFragmentManager.beginTransaction().apply{
            replace(R.id.fll_fragment, aFragment)
            commit()
        }



    }

    override fun loginSuccessful(s: String) {
        val bundle = Bundle()
        bundle.putString("login", s)
        val bFragment = BFragment()
        bFragment.arguments = bundle
        supportFragmentManager.beginTransaction().apply{
            replace(R.id.fll_fragment, bFragment)
            commit()
        }

    }

    override fun loginUnSuccessful(s: String) {
        val bundle = Bundle()
        bundle.putString("failed", s)
        val aFragment = AFragment()
        aFragment.arguments = bundle
        supportFragmentManager.beginTransaction().apply{
            replace(R.id.fll_fragment, aFragment)
            commit()
        }
    }

    override fun gotoLoginFragment() {
        val aFragment = AFragment()

        supportFragmentManager.beginTransaction().apply{
            replace(R.id.fll_fragment, aFragment)
            commit()
        }
    }


}